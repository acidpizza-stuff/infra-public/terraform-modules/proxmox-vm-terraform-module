# Proxmox VM Terraform Module

Migrated from [telmate provider](https://registry.terraform.io/providers/Telmate/proxmox/latest/docs) to [bpg provider](https://registry.terraform.io/providers/bpg/proxmox/latest/docs) as telmate provider seems to be [not supported anymore](https://github.com/Telmate/terraform-provider-proxmox/issues/863).

[[_TOC_]]


## Module Usage

```
source = "git::https://gitlab.com/acidpizza-stuff/infra/terraform-modules/proxmox-vm-terraform-module.git//module/ubuntu"
```

## Issues

### Cannot modify number of disks

Due to a limitation of the provider, changing the number of disks will cause the VM to be recreated. However, we can modify the size of existing disks.

### Terraform cannot sync after changing disk size

After changing the first disk size, terraform will be unable to sync state as the old size is being reflected.

Workaround:
- Go to the UI ➔ VM ➔ Hardware
- Double-click the hard disk (there is a black and orange entry in the same entry)
- Set the cache to any value ➔ OK
- Set the cache back to the original value (no cache) ➔ OK
- This will force proxmox to sync the disk size correctly


## Telmate Provider (Legacy)

All sections below are legacy issues related to the telmate provider.

### Preparation of cloud-ready image (Not Used)

```bash
# Install libguestfs-tools on Proxmox server.
apt-get install libguestfs-tools
# Install qemu-guest-agent on Ubuntu image.
virt-customize -a focal-server-cloudimg-amd64.img --install qemu-guest-agent
# Enable password authentication in the template. Obviously, not recommended for except for testing.
virt-customize -a focal-server-cloudimg-amd64.img --run-command "sed -i 's/.*PasswordAuthentication.*/PasswordAuthentication yes/g' /etc/ssh/sshd_config"
```

### Cloud-Init Process

Reference: https://yetiops.net/posts/proxmox-terraform-cloudinit-saltstack-prometheus/#define-an-instance

In general, cloud init is not advisable for proxmox as the userdata file must be copied to the proxmox node in order for it to be read by the VM that is being created. Care must be taken not to refer to other userdata files that were copied by other terraform code. It would be better to use post-provisoining null resources instead, since there will be no dependency on the proxmox node's filesystem.


```terraform
# Create cloudconfig file from template
data "template_file" "cloud_init_deb10_vm-01" {
  template  = "${file("${path.module}/files/cloud_init_deb10.cloud_config")}"

  vars = {
    ssh_key = file("~/.ssh/id_rsa.pub")
    hostname = "vm-01"
    domain = "yetiops.lab"
  }
}

# Create a local copy of the cloudconfig file
resource "local_file" "cloud_init_deb10_vm-01" {
  content   = data.template_file.cloud_init_deb10_vm-01.rendered
  filename  = "${path.module}/files/user_data_cloud_init_deb10_vm-01.cfg"
}

# Transfer the cloudconfig file to the Proxmox Host
resource "null_resource" "cloud_init_deb10_vm-01" {
  connection {
    type    = "ssh"
    user    = "root"
    private_key = file("~/.ssh/id_rsa")
    host    = "10.15.31.7"
  }

  provisioner "file" {
    source       = local_file.cloud_init_deb10_vm-01.filename
    destination  = "/var/lib/vz/snippets/cloud_init_deb10_vm-01.yml"
  }
}


# Create the VM
resource "proxmox_vm_qemu" "vm-01" {
  ## Wait for the cloud-config file to exist
  depends_on = [
    null_resource.cloud_init_deb10_vm-01
  ]

  # Cloud init options
  cicustom = "user=local:snippets/cloud_init_deb10_vm-01.yml"
  ipconfig0 = "ip=10.15.31.99/24,gw=10.15.31.253"

  ...
}
```

### Issues

> `TASK ERROR: virtio0 - cloud-init drive is already attached at 'ide0'`

There seems to be some race condition that causes this error to occur sometimes when provisioning the VM. It will be solved when reapplying the terraform code. Was unable to solve this issue.

Attempts:
- use `boot: order=virtio0;ide0` in both packer code and terraform code
- use `seabios`, but was not successful in getting autoinstall working correctly for seabios on ubuntu `20.04.5`. Seems like the newer version doesn't work the same way.
  - escape semicolon in kernel cmdline: `"autoinstall ds='nocloud-net\;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/' ---"`

**Github Issue**: https://github.com/Telmate/terraform-provider-proxmox/issues/704
- Workaround: Stick to version 2.9.11
- Suggested solutions:
  - [Specify slot = 2](https://github.com/Telmate/terraform-provider-proxmox/issues/704#issuecomment-1628961851) on the disk configuration, but it [seems like it doesn't work](https://github.com/Telmate/terraform-provider-proxmox/issues/704#issuecomment-1646519281)
  - [Remove cloudinit from packer template](https://github.com/Telmate/terraform-provider-proxmox/issues/704#issuecomment-1645291953) and specify `cloudinit_cdrom_storage = "local-lvm"` in terraform to recreate the cloudinit drive