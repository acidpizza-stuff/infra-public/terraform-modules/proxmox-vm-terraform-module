#### Proxmox Settings #####
variable "proxmox_password" {
  type        = string
  description = "Password for proxmox."
}

#### VM Post Config #####
variable "user_public_key_path" {
  type        = string
  description = "The path to public key for the os user." 
}
