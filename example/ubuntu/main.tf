locals {
  proxmox_node     = "pve"
  proxmox_host     = "pve.lan"
  proxmox_username = "root"
  proxmox_realm    = "pam"
}

module "ubuntu_vm" {
  count = 1

  source = "../../module/ubuntu"
  # source = "git::https://gitlab.com/acidpizza-stuff/infra/terraform-modules/proxmox-vm-terraform-module.git//module/ubuntu"

  # --- Proxmox Settings ---
  proxmox_node = local.proxmox_node

  # --- VM Settings ---
  vm_template_name = "ubuntu-server-22-04-amd64"
  vm_name = "test-vm-${count.index}"
  vm_description = "test description"

  vm_cores = 2
  vm_memory = 4
  vm_disks = [ 20, 10 ]
  # vm_network_ipv4 = "192.168.1.140/24"  # Don't set this for dhcp
  # vm_network_gateway = "192.168.1.254"  # Don't set this for dhcp

  # os_initial_password = "mypassword"

  # --- VM Post Config ---
  user_public_key = file(var.user_public_key_path)
}
