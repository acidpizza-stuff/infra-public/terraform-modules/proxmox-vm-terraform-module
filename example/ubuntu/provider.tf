terraform {
  required_providers {
    proxmox = {
      source = "bpg/proxmox"
      version = "0.61.1"
    }
  }
}

provider "proxmox" {
  endpoint = "https://${local.proxmox_host}:8006/"
  username = "${local.proxmox_username}@${local.proxmox_realm}"
  password = var.proxmox_password
  insecure = true
}
