output "ip" {
  value = [ for vm in module.ubuntu_vm : vm.ip ]
}

output "vm" {
  value = [ for vm in module.ubuntu_vm : vm.vm ]
  sensitive = true
}

output "vm_password" {
  value = [ for vm in module.ubuntu_vm : vm.vm_password ]
  sensitive = true
}