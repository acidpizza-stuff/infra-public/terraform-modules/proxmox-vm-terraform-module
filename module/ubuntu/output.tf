output "ip" {
  value = module.ubuntu_base.ip
}

output "vm" {
  value = module.ubuntu_base.vm
}

output "vm_password" {
  value     = module.ubuntu_base.vm_password
  sensitive = true
}