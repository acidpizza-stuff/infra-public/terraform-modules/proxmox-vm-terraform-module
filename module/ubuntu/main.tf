locals {
}

module "ubuntu_base" {
  source = "../ubuntu_base"

  # --- Proxmox Settings ---
  proxmox_node = var.proxmox_node

  # --- VM Settings ---
  vm_id                  = var.vm_id
  vm_template_name       = var.vm_template_name
  vm_template_full_clone = var.vm_template_full_clone

  vm_name          = var.vm_name
  vm_description   = var.vm_description

  vm_start_on_boot = var.vm_start_on_boot
  vm_started       = var.vm_started
  vm_bios          = var.vm_bios

  ### CPU, Memory ###
  vm_sockets = var.vm_sockets
  vm_cores   = var.vm_cores
  vm_memory  = var.vm_memory

  ### Disk ###
  vm_disks        = var.vm_disks
  vm_storage_pool = var.vm_storage_pool

  ### Network ###
  vm_network_bridge      = var.vm_network_bridge
  vm_network_ipv4        = var.vm_network_ipv4
  vm_network_gateway     = var.vm_network_gateway
  vm_network_dns_servers = var.vm_network_dns_servers
  vm_network_dns_domain  = var.vm_network_dns_domain

  ### Cloud-init ###
  os_initial_password  = var.os_initial_password
}

resource terraform_data "vm_postconfig" {
  depends_on = [
    module.ubuntu_base
  ]
  connection {
    type = "ssh"
    user = "ubuntu"
    password = module.ubuntu_base.vm_password
    host = module.ubuntu_base.ip
  }
  provisioner "remote-exec" {
    inline = [
      "#!/bin/bash",
      "set -euo pipefail",
      # Check if this code has run before
      <<-EOF
      if getent passwd | grep ${var.os_username}; then
        echo "user [${var.os_username}] exists. Not running vm_postconfig."
        exit 0
      fi
      EOF
      ,
      # Wait for cloud init to complete
      <<-EOF
      (
        sudo tail -f /var/log/cloud-init-output.log &
        TAIL_PID=$!
        trap "sudo kill $TAIL_PID" EXIT
        sudo cloud-init status --wait
      )
      EOF
      ,
      # Add user with sudo rights
      "sudo useradd -m -s /bin/bash ${var.os_username}",
      "echo \"${var.os_username} ALL=(ALL) NOPASSWD:ALL\" | sudo tee /etc/sudoers.d/90-${var.os_username} > /dev/null",
      # Setup ssh folder and add authorized key for user
      "sudo mkdir -p /home/${var.os_username}/.ssh",
      "sudo chmod 700 /home/${var.os_username}/.ssh",
      "echo \"${var.user_public_key}\" | sudo tee /home/${var.os_username}/.ssh/authorized_keys",
      "sudo chmod 600 /home/${var.os_username}/.ssh/authorized_keys",
      "sudo chown -R ${var.os_username}:${var.os_username} /home/${var.os_username}/",
      # Ban password authentication over SSH
      "cat /etc/ssh/sshd_config | grep '^PasswordAuthentication no$' || echo 'PasswordAuthentication no' | sudo tee -a /etc/ssh/sshd_config",
      # Set Hostname
      "sudo hostnamectl set-hostname ${var.vm_name}",
      # Resize hard disk for free space
      "sudo /etc/auto_resize_vda.sh",
      # Update OS
      "sudo apt update && sudo apt upgrade -y",
    ]
  }
  provisioner "remote-exec" {
    # Workaround to allow reboot which causes provisioner to error out
    inline = [
      #!/bin/bash
      "([ -f /var/run/reboot-required ] && sudo shutdown -r now) || true",
    ]
    on_failure = continue
  }
  provisioner "remote-exec" {
    # Wait until server has rebooted
    inline = [
      "#!/bin/bash",
      "set -euo pipefail",
      "while [ -f /run/systemd/shutdown/scheduled ]; do echo 'waiting for server to reboot...'; sleep 1; done",
      "echo 'server has rebooted'",
    ]
  }
}
