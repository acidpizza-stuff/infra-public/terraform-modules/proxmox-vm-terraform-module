#### Proxmox Settings #####
variable "proxmox_node" {
  type        = string
  description = "Which node in the Proxmox cluster to start the virtual machine on during creation."
  default     = "pve"
  nullable    = false
}

#### VM Settings #####
variable "vm_id" {
  type        = number
  description = "The ID of the VM in Proxmox. `null` indicates the next available ID."
  default     = null
}

variable "vm_template_name" {
  type        = string
  description = "Name of the VM template to clone from."
}

variable "vm_template_full_clone" {
  type        = bool
  description = "Set to true to create a full clone, or false to create a linked clone."
  default     = false
  nullable    = false
}

variable "vm_name" {
  type        = string
  description = "The virtual machine name."
}

variable "vm_description" {
  type        = string
  description = "The virtual machine description."
}

variable "vm_start_on_boot" {
  type        = bool
  description = "Specifies whether the VM will be started during system bootup."
  default     = true
  nullable    = false
}

variable "vm_started" {
  type        = bool
  description = "Specifies whether the VM will be started after provisioning ."
  default     = true
  nullable    = false
}

variable "vm_bios" {
  type        = string
  description = "Specifies the BIOS to use."
  default     = "ovmf"
  nullable    = false

  validation {
    condition = (
      var.vm_bios == "ovmf"    ||
      var.vm_bios == "seabios"
    )
    error_message = "The vm_bios must be one of the following: [ovmf, seabios]"
  }
}

variable "vm_sockets" {
  type        = number
  description = "How many CPU sockets to give the virtual machine."
  default     = 1
  nullable    = false
}

variable "vm_cores" {
  type        = number
  description = "How many CPU cores to give the virtual machine."
  default     = 1
  nullable    = false
}

variable "vm_memory" {
  type        = number
  description = "How much memory, in gigabytes, to give the virtual machine."
  default     = 1
  nullable    = false
}

variable "vm_disks" {
  type        = list(number)
  description = "A list of sizes (GB) of disks to attach to the VM"
  default     = []
  nullable    = false
}

variable "vm_storage_pool" {
  type        = string
  description = "Name of the Proxmox storage pool to store the virtual machine disks on."
  default     = "local-lvm"
  nullable    = false
}

variable "vm_network_bridge" {
  type        = string
  description = "Which Proxmox bridge to attach the adapter to."
  default     = "vmbr0"
  nullable    = false
}

variable "vm_network_ipv4" {
  type        = string
  description = "The IP address with CIDR block for the primary network interface (eg. 192.168.1.1/24). Also can set as dhcp."
  default     = "dhcp"
  nullable    = false
}

variable "vm_network_gateway" {
  type        = string
  description = "The network gateway to use for the primary network interface. Ignored if vm_network_ipv4 = dhcp."
  default     = null
}

variable "vm_network_dns_servers" {
  type        = list(string)
  description = "The dns servers to use for the primary network interface."
  default     = null
}

variable "vm_network_dns_domain" {
  type        = string
  description = "The dns domain to use for the primary network interface."
  default     = null
}

variable "os_initial_password" {
  type        = string
  description = "Set an initial OS password. If not set, a random password will be used."
  default     = null
}