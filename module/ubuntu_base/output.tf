output "ip" {
  # workaround to get the default VM IP address
  value = try(
    [for ip in flatten(proxmox_virtual_environment_vm.ubuntu_base.ipv4_addresses) : ip if startswith(ip, "192.168")][0],
    ""
  )
}

output "vm" {
  value = proxmox_virtual_environment_vm.ubuntu_base
}

output "vm_password" {
  value     = var.os_initial_password != null ? var.os_initial_password : random_password.vm_password[0].result
  sensitive = true
}