locals {
  # network_dhcp_config = <<-EOF
  #   auto eth0
  #   iface eth0 inet dhcp
  #   EOF

  # network_static_config = <<-EOF
  #   auto eth0
  #   iface eth0 inet static
  #     address ${var.vm_network_ipv4}
  #     netmask 255.255.255.0
  #     broadcast 192.168.0.255
  #     gateway ${var.vm_network_gateway}
  #     dns-nameservers 192.168.1.130
  #   EOF

  # Only choose the VM whose name is the vm_template_name
  vm_template_id = try(
    [ for vm in data.proxmox_virtual_environment_vms.vm_template.vms : vm.vm_id if vm.name == var.vm_template_name ][0],
    file("Error: vm_template_name ${var.vm_template_name} cannot be found.")
  )
}

data "proxmox_virtual_environment_vms" "vm_template" {
  node_name = var.proxmox_node
  tags      = [var.vm_template_name]
}

resource "proxmox_virtual_environment_vm" "ubuntu_base" {
  ### Proxmox Settings ###
  node_name  = var.proxmox_node

  ### VM Settings ###
  vm_id       = var.vm_id
  clone {
    vm_id     = local.vm_template_id
    full      = var.vm_template_full_clone  # undocumented https://github.com/bpg/terraform-provider-proxmox/issues/551
  }
  name        = var.vm_name
  description = var.vm_description
  tags        = []

  on_boot     = var.vm_start_on_boot
  started     = var.vm_started
  boot_order  = [  # boot from virtio0 (boot disk), and ide2 (cloudinit)
    "virtio0",
    "ide2",
    "net0", 
  ]
  operating_system {
    type      = "l26"
  }
  agent {  # qemu agent
    enabled   = true
  }
  machine     = "q35"

  ### CPU, Memory ###
  cpu {
    architecture = "x86_64"
    sockets      = var.vm_sockets
    cores        = var.vm_cores
    type         = "host"
  }
  memory {
    dedicated    = var.vm_memory * 1024
  }

  ### Disk ###
  scsi_hardware    = "virtio-scsi-single"
  bios             = var.vm_bios
  ## required if bios = ovmf, but gives errors because efi disk was provisioned in packer template:
  ## "resizing of efidisks is not supported"
  # dynamic efi_disk {
  #   for_each       = var.vm_bios == "ovmf" ? [1] : []
  #   content {
  #     datastore_id = var.vm_storage_pool
  #     file_format  = "raw"
  #     type         = "4m"
  #   }
  # }
  dynamic "disk" {
    for_each       = var.vm_disks
    content {
      interface    = "virtio${disk.key}"
      datastore_id = var.vm_storage_pool
      size         = disk.value
      file_format  = "raw"
      iothread     = true
    }
  }

  ### Network ###
  network_device {
    model  = "virtio"
    bridge = var.vm_network_bridge
  }

  ### VGA ###
  vga {
    memory = 16
    type = "std"
  }

  ### Cloud-init ###
  initialization {
    datastore_id = "local-lvm"
    interface    = "ide2"  # cloud-init drive
    ip_config {
      ipv4 {
        address = var.vm_network_ipv4
        gateway = (var.vm_network_ipv4 == "dhcp") ? null : var.vm_network_gateway
      }
    }
    dynamic dns {
      for_each = var.vm_network_dns_servers != null ? [1] : []
      content {
        servers = var.vm_network_dns_servers
        domain = var.vm_network_dns_domain
      }
    }
    user_account {
      username = "ubuntu"
      password = var.os_initial_password != null ? var.os_initial_password : random_password.vm_password[0].result
    }
  }

  lifecycle {
    ignore_changes = [
      clone,
      boot_order,
      disk[0].discard,  # adding additional entries as we can't use wildcard here
      disk[1].discard,
      disk[2].discard,
      disk[3].discard,
      disk[4].discard,
      disk[5].discard,
      efi_disk,
      initialization,
      started,
      usb,
    ]
  }
}

resource random_password "vm_password" {
  count = var.os_initial_password == null ? 1 : 0

  length           = 16
  override_special = "_%@"
  special          = true
}